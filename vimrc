set nocompatible
filetype off

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

Bundle 'gmarik/vundle'

" General Vim Stuff
Bundle 'tpope/vim-git'
Bundle 'tpope/vim-fugitive'
Bundle 'mileszs/ack.vim'
Bundle 'Lokaltog/vim-powerline'
Bundle 'Lokaltog/vim-easymotion'
Bundle 'tpope/vim-surround'
Bundle 'tpope/vim-commentary'
Bundle 'tpope/vim-repeat'
Bundle 'Shougo/vimproc'
Bundle 'scrooloose/syntastic'
Bundle 'rosenfeld/conque-term'
Bundle 'kien/ctrlp.vim'
Bundle 'mattn/emmet-vim'
Bundle 'Valloric/YouCompleteMe'
Bundle 'SirVer/ultisnips'
Bundle 'taglist.vim'
Bundle 'vcscommand.vim'
Bundle 'kien/rainbow_parentheses.vim'

"" Snipmate Dependency
Bundle "MarcWeber/vim-addon-mw-utils"
Bundle "tomtom/tlib_vim"

" Colorschemes
Bundle "altercation/vim-colors-solarized"
Bundle "tpope/vim-vividchalk"
Bundle 'tomasr/molokai'
Bundle "wombat256.vim"


" Tabular
Bundle "godlygeek/tabular"
map <Leader>t= :Tabularize /=<CR>
map <Leader>t: :Tabularize /:\zs<CR>



" Indent Guides
Bundle "nathanaelkane/vim-indent-guides"
let g:indent_guides_start_level=3
let g:indent_guides_guide_size=1
let g:indent_guides_auto_colors = 1


" Javascript Modes
Bundle 'pangloss/vim-javascript'


" Haskell Modes
Bundle 'eagletmt/ghcmod-vim'
Bundle 'lukerandall/haskellmode-vim'
Bundle 'Shougo/neocomplcache'
Bundle 'ujihisa/neco-ghc'


" Python Modes
Bundle 'klen/python-mode'
Bundle 'alfredodeza/pytest.vim'

" PHP Modes
let g:syntastic_php_checkers=['php', 'tidy', 'phpmd']


" Javascript
Bundle "marijnh/tern_for_vim"

" Coffeescript
Bundle 'kchmck/vim-coffee-script'


"Scala Modes
Bundle 'derekwyatt/vim-scala'


" Zencoding
let &rtp = '~/.vim/,'.&rtp
let g:user_emmet_leader_key = "<c-o>"



au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces


" Jade
Bundle 'digitaltoad/vim-jade'

" UltiSnips
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-n>"
let g:UltiSnipsJumpBackwardTrigger="<c-p>"

let g:ycm_key_list_select_completion = ['<Down>']
let g:ycm_key_list_previous_completion = ['<Up>']

let g:ycm_complete_in_strings = 1
let g:ycm_collect_identifiers_from_comments_and_strings = 1
let g:ycm_collect_identifiers_from_tags_files = 1
let g:ycm_seed_identifiers_with_syntax = 1
set completeopt=menuone

set backspace=indent,eol,start



" Taglist
nnoremap <F6> :TlistToggle<CR>




filetype plugin indent on
syntax on

set t_Co=256
let g:Powerline_symbols = 'fancy'
set nu
set wrap
set cc=80
set cursorline

" gets rid of input lag with Esc key in terminal
set ttimeout
set ttimeoutlen=100


let mapleader = '\'

"" Window Jumping Maps
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

"" Commands
" map ; :


"" Relative Line Numbers
set relativenumber

" Always show line numbers, but only in current window.
:au WinEnter * :setlocal relativenumber
:au WinLeave * :setlocal norelativenumber
" Absolute Line Numbers in Insert Mode
:au InsertEnter * :set number
:au InsertLeave * :set relativenumber


"" Remap Directions
nnoremap q h
nnoremap Q H
nnoremap h x
nnoremap H X
nnoremap x l
nnoremap X L
nnoremap l q
nnoremap L Q


" Git Fugitive Mapping
map <leader>g :Git
map <leader>gc :Gcommit<CR>
map <leader>gca :Gcommit --amend<CR>
map <leader>gd :Gdiff<CR>
map <leader>ga :Gwrite<CR>
map <leader>gs :Gstatus<CR>
map <leader>gh :Gcd<CR>


" VCS Command Mapping
let g:VCSCommandMapPrefix = '<leader>v'


" CtrlP
" set wildignore+=,*/tmp/*,*.so/*.swp,*.zip,*/.env/*,*.pyc
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.git|\.hg|\.svn|\.env|\.sass-cache|\.ropeproject|node_modules|ai-cache|virtualenv|target)$',
  \ 'file': '\v\.(exe|so|dll|pyc|fuse_h*)$',
  \ }
let g:ctrlp_root_markers = ['.git', '.hg', 'trunk']
let g:ctrlp_dotfiles = 0


nnoremap <F2> :CtrlPMRU<CR>
nnoremap <F3> :CtrlP<CR>
nnoremap <F4> :CtrlPBuffer<CR>
nnoremap <F5> :CtrlPTag<CR>


xmap <leader>~  <Plug>Commentary
nmap <leader>~  <Plug>Commentary
nmap <leader>~~ <Plug>CommentaryLine
nmap <leader>~~u <Plug>CommentaryUndo


"" Filetype Overrides
au BufNewFile,BufRead *.as set filetype=actionscript
au Bufenter *.hs compiler ghc



" Haskell
let g:haddock_browser = "/usr/bin/firefox"


" SASS
autocmd BufNewFile,BufRead *.sass             set ft=sass.css
autocmd BufNewFile,BufRead *.pp               set ft=puppet

function! s:MKDir(...)
    if         !a:0 
           \|| stridx('`+', a:1[0])!=-1
           \|| a:1=~#'\v\\@<![ *?[%#]'
           \|| isdirectory(a:1)
           \|| filereadable(a:1)
           \|| isdirectory(fnamemodify(a:1, ':p:h'))
        return
    endif
    return mkdir(fnamemodify(a:1, ':p:h'), 'p')
endfunction
command! -bang -bar -nargs=? -complete=file E :call s:MKDir(<f-args>) | e<bang> <args>



command! RefreshConfig so ~/.vim/vimrc | so ~/.vim/gvimrc
map <leader>rc :RefreshConfig<CR>
map <leader>rp :CtrlPClearAllCaches<CR>




let g:user_zen_settings = {
\    'lang': "en",
\    'charset': "UTF-8",
\    'css': {
\        'snippets': {
\            'poss': 'position:static;',
\            'posa': 'position:absolute;',
\            'posr': 'position:relative;',
\            'posf': 'position:fixed;',
\            'ta': 'top:auto;',
\            'ra': 'right:auto;',
\            'ba': 'bottom:auto;',
\            'la': 'left:auto;',
\            'za': 'z-index:auto;',
\            'fln': 'float:none;',
\            'fll': 'float:left;',
\            'flr': 'float:right;',
\            'cln': 'clear:none;',
\            'cll': 'clear:left;',
\            'clr': 'clear:right;',
\            'clb': 'clear:both;',
\            'dn': 'display:none;',
\            'db': 'display:block;',
\            'di': 'display:inline;',
\            'dib': 'display:inline-block;',
\            'dli': 'display:list-item;',
\            'dri': 'display:run-in;',
\            'dcp': 'display:compact;',
\            'dtb': 'display:table;',
\            'ditb': 'display:inline-table;',
\            'dtbcp': 'display:table-caption;',
\            'dtbcl': 'display:table-column;',
\            'dtbclg': 'display:table-column-group;',
\            'dtbhg': 'display:table-header-group;',
\            'dtbfg': 'display:table-footer-group;',
\            'dtbr': 'display:table-row;',
\            'dtbrg': 'display:table-row-group;',
\            'dtbc': 'display:table-cell;',
\            'drb': 'display:ruby;',
\            'drbb': 'display:ruby-base;',
\            'drbbg': 'display:ruby-base-group;',
\            'drbt': 'display:ruby-text;',
\            'drbtg': 'display:ruby-text-group;',
\            'vv': 'visibility:visible;',
\            'vh': 'visibility:hidden;',
\            'vc': 'visibility:collapse;',
\            'ovv': 'overflow:visible;',
\            'ovh': 'overflow:hidden;',
\            'ovs': 'overflow:scroll;',
\            'ova': 'overflow:auto;',
\            'ovxv': 'overflow-x:visible;',
\            'ovxh': 'overflow-x:hidden;',
\            'ovxs': 'overflow-x:scroll;',
\            'ovxa': 'overflow-x:auto;',
\            'ovyv': 'overflow-y:visible;',
\            'ovyh': 'overflow-y:hidden;',
\            'ovys': 'overflow-y:scroll;',
\            'ovya': 'overflow-y:auto;',
\            'ovsa': 'overflow-style:auto;',
\            'ovss': 'overflow-style:scrollbar;',
\            'ovsp': 'overflow-style:panner;',
\            'ovsm': 'overflow-style:move;',
\            'ovsmq': 'overflow-style:marquee;',
\            'cpa': 'clip:auto;',
\            'cpr': 'clip:rect(|);',
\            'bxzcb': 'box-sizing:content-box;',
\            'bxzbb': 'box-sizing:border-box;',
\            'bxshn': 'box-shadow:none;',
\            'bxshw': '-webkit-box-shadow:0 0 0 #000;',
\            'bxshm': '-moz-box-shadow:0 0 0 0 #000;',
\            'ma': 'margin:auto;',
\            'wa': 'width:auto;',
\            'ha': 'height:auto;',
\            'mawn': 'max-width:none;',
\            'mahn': 'max-height:none;',
\            'on': 'outline:none;',
\            'oci': 'outline-color:invert;',
\            'bd+': 'border:1px solid #000;',
\            'bdn': 'border:none;',
\            'bdbkc': 'border-break:close;',
\            'bdclc': 'border-collapse:collapse;',
\            'bdcls': 'border-collapse:separate;',
\            'bdblrz': 'border-bottom-left-radius:|;',
\            'bdrz:w': '-webkit-border-radius:|;',
\            'bdrz:m': '-moz-border-radius:|;',
\            'bg': 'background:|;',
\            'bg+': 'background:#FFF url(|) 0 0 no-repeat;',
\            'bgn': 'background:none;',
\            'c': 'color:#000;',
\            'tbla': 'table-layout:auto;',
\            'tblf': 'table-layout:fixed;',
\            'cpst': 'caption-side:top;',
\            'cpsb': 'caption-side:bottom;',
\            'ecs': 'empty-cells:show;',
\            'ech': 'empty-cells:hide;',
\            'lisn': 'list-style:none;',
\            'lispi': 'list-style-position:inside;',
\            'lispo': 'list-style-position:outside;',
\            'listn': 'list-style-type:none;',
\            'listd': 'list-style-type:disc;',
\            'listc': 'list-style-type:circle;',
\            'lists': 'list-style-type:square;',
\            'listdc': 'list-style-type:decimal;',
\            'listdclz': 'list-style-type:decimal-leading-zero;',
\            'listlr': 'list-style-type:lower-roman;',
\            'listur': 'list-style-type:upper-roman;',
\            'lisin': 'list-style-image:none;',
\            'qn': 'quotes:none;',
\            'qru': 'quotes:''\00AB'' ''\00BB'' ''\201E'' ''\201C'';',
\            'qen': 'quotes:''\201C'' ''\201D'' ''\2018'' ''\2019'';',
\            'ctn': 'content:normal;',
\            'ctoq': 'content:open-quote;',
\            'ctnoq': 'content:no-open-quote;',
\            'ctcq': 'content:close-quote;',
\            'ctncq': 'content:no-close-quote;',
\            'cta': 'content:attr(|);',
\            'ctc': 'content:counter(|);',
\            'ctcs': 'content:counters(|);',
\            'vasup': 'vertical-align:super;',
\            'vat': 'vertical-align:top;',
\            'vatt': 'vertical-align:text-top;',
\            'vam': 'vertical-align:middle;',
\            'vabl': 'vertical-align:baseline;',
\            'vab': 'vertical-align:bottom;',
\            'vatb': 'vertical-align:text-bottom;',
\            'vasub': 'vertical-align:sub;',
\            'tal': 'text-align:left;',
\            'tac': 'text-align:center;',
\            'tar': 'text-align:right;',
\            'tala': 'text-align-last:auto;',
\            'tall': 'text-align-last:left;',
\            'talc': 'text-align-last:center;',
\            'talr': 'text-align-last:right;',
\            'tdn': 'text-decoration:none;',
\            'tdu': 'text-decoration:underline;',
\            'tdo': 'text-decoration:overline;',
\            'tdl': 'text-decoration:line-through;',
\            'ten': 'text-emphasis:none;',
\            'teac': 'text-emphasis:accent;',
\            'tedt': 'text-emphasis:dot;',
\            'tec': 'text-emphasis:circle;',
\            'teds': 'text-emphasis:disc;',
\            'teb': 'text-emphasis:before;',
\            'tea': 'text-emphasis:after;',
\            'tha': 'text-height:auto;',
\            'thf': 'text-height:font-size;',
\            'tht': 'text-height:text-size;',
\            'thm': 'text-height:max-size;',
\            'ti-': 'text-indent:-9999px;',
\            'tja': 'text-justify:auto;',
\            'tjiw': 'text-justify:inter-word;',
\            'tjii': 'text-justify:inter-ideograph;',
\            'tjic': 'text-justify:inter-cluster;',
\            'tjd': 'text-justify:distribute;',
\            'tjk': 'text-justify:kashida;',
\            'tjt': 'text-justify:tibetan;',
\            'to+': 'text-outline:0 0 #000;',
\            'ton': 'text-outline:none;',
\            'trn': 'text-replace:none;',
\            'ttn': 'text-transform:none;',
\            'ttc': 'text-transform:capitalize;',
\            'ttu': 'text-transform:uppercase;',
\            'ttl': 'text-transform:lowercase;',
\            'twn': 'text-wrap:normal;',
\            'twno': 'text-wrap:none;',
\            'twu': 'text-wrap:unrestricted;',
\            'tws': 'text-wrap:suppress;',
\            'tsh+': 'text-shadow:0 0 0 #000;',
\            'tshn': 'text-shadow:none;',
\            'whsn': 'white-space:normal;',
\            'whsp': 'white-space:pre;',
\            'whsnw': 'white-space:nowrap;',
\            'whspw': 'white-space:pre-wrap;',
\            'whspl': 'white-space:pre-line;',
\            'whscn': 'white-space-collapse:normal;',
\            'whsck': 'white-space-collapse:keep-all;',
\            'whscl': 'white-space-collapse:loose;',
\            'whscbs': 'white-space-collapse:break-strict;',
\            'whscba': 'white-space-collapse:break-all;',
\            'wobn': 'word-break:normal;',
\            'wobk': 'word-break:keep-all;',
\            'wobl': 'word-break:loose;',
\            'wobbs': 'word-break:break-strict;',
\            'wobba': 'word-break:break-all;',
\            'wownm': 'word-wrap:normal;',
\            'wown': 'word-wrap:none;',
\            'wowu': 'word-wrap:unrestricted;',
\            'wows': 'word-wrap:suppress;',
\            'fwn': 'font-weight:normal;',
\            'fwb': 'font-weight:bold;',
\            'fwbr': 'font-weight:bolder;',
\            'fwlr': 'font-weight:lighter;',
\            'fsn': 'font-style:normal;',
\            'fsi': 'font-style:italic;',
\            'fso': 'font-style:oblique;',
\            'fvn': 'font-variant:normal;',
\            'fvsc': 'font-variant:small-caps;',
\            'fzan': 'font-size-adjust:none;',
\            'ffs': 'font-family:serif;',
\            'ffss': 'font-family:sans-serif;',
\            'ffc': 'font-family:cursive;',
\            'fff': 'font-family:fantasy;',
\            'ffm': 'font-family:monospace;',
\            'fefn': 'font-effect:none;',
\            'fefeg': 'font-effect:engrave;',
\            'fefeb': 'font-effect:emboss;',
\            'fefo': 'font-effect:outline;',
\            'fempb': 'font-emphasize-position:before;',
\            'fempa': 'font-emphasize-position:after;',
\            'femsn': 'font-emphasize-style:none;',
\            'femsac': 'font-emphasize-style:accent;',
\            'femsdt': 'font-emphasize-style:dot;',
\            'femsc': 'font-emphasize-style:circle;',
\            'femsds': 'font-emphasize-style:disc;',
\            'fsma': 'font-smooth:auto;',
\            'fsmn': 'font-smooth:never;',
\            'fsmaw': 'font-smooth:always;',
\            'fstn': 'font-stretch:normal;',
\            'fstuc': 'font-stretch:ultra-condensed;',
\            'fstec': 'font-stretch:extra-condensed;',
\            'fstc': 'font-stretch:condensed;',
\            'fstsc': 'font-stretch:semi-condensed;',
\            'fstse': 'font-stretch:semi-expanded;',
\            'fste': 'font-stretch:expanded;',
\            'fstee': 'font-stretch:extra-expanded;',
\            'fstue': 'font-stretch:ultra-expanded;',
\            'opie': 'filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=100);',
\            'opms': '-ms-filter:''progid:DXImageTransform.Microsoft.Alpha(Opacity=100)'';',
\            'rzn': 'resize:none;',
\            'rzb': 'resize:both;',
\            'rzh': 'resize:horizontal;',
\            'rzv': 'resize:vertical;',
\            'cura': 'cursor:auto;',
\            'curd': 'cursor:default;',
\            'curc': 'cursor:crosshair;',
\            'curha': 'cursor:hand;',
\            'curhe': 'cursor:help;',
\            'curm': 'cursor:move;',
\            'curp': 'cursor:pointer;',
\            'curt': 'cursor:text;',
\            'pgbbau': 'page-break-before:auto;',
\            'pgbbal': 'page-break-before:always;',
\            'pgbbl': 'page-break-before:left;',
\            'pgbbr': 'page-break-before:right;',
\            'pgbiau': 'page-break-inside:auto;',
\            'pgbiav': 'page-break-inside:avoid;',
\            'pgbaau': 'page-break-after:auto;',
\            'pgbaal': 'page-break-after:always;',
\            'pgbal': 'page-break-after:left;',
\            'pgbar': 'page-break-after:right;',
\        },
\        'filters': 'fc'
\    }
\}


set guioptions-=m
set guioptions-=T
set guioptions-=r


colorscheme solarized
set guifont=Inconsolata\ 10
let Powerline_symbols = 'fancy'

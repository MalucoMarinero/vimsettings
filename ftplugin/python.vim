setlocal tabstop=4
setlocal shiftwidth=4
setlocal expandtab
setlocal autoindent
setlocal smarttab
setlocal formatoptions=croq1

let g:pep8_map='<leader>8'

" Pytest Bindings
" Execute the tests
nmap <silent><Leader>tf <Esc>:Pytest file<CR>
nmap <silent><Leader>tfp <Esc>:Pytest file --pdb<CR>
nmap <silent><Leader>tfs <Esc>:Pytest file -s<CR>
nmap <silent><Leader>tc <Esc>:Pytest class<CR>
nmap <silent><Leader>tcp <Esc>:Pytest class --pdb<CR>
nmap <silent><Leader>tcs <Esc>:Pytest class -s<CR>
nmap <silent><Leader>tm <Esc>:Pytest method<CR>
nmap <silent><Leader>tmp <Esc>:Pytest method --pdb<CR>
nmap <silent><Leader>tms <Esc>:Pytest method -s<CR>
" cycle through test errors
nmap <silent><Leader>tn <Esc>:Pytest next<CR>
nmap <silent><Leader>tp <Esc>:Pytest previous<CR>
nmap <silent><Leader>te <Esc>:Pytest error<CR>

nmap <silent><Leader>qn <Esc>:cn<CR>
nmap <silent><Leader>qp <Esc>:cp<CR>


" Rope Bindings
map <leader>ro :RopeOrganizeImports<CR>
map <leader>rpo :RopeOpenProject<CR>
map <leader>rpc :RopeCloseProject<CR>
map <leader>j :RopeGotoDefinition<CR>
map <leader>rr :RopeRename<CR>
map <leader>rmm :RopeMoveCurrentModule<CR>
map <leader>rmr :RopeRenameCurrentModule<CR>
map <leader>re :RopeExtractMethod<CR>
